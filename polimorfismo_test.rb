#!/usr/bin/env ruby
gem 'minitest', '>= 5.0.0'
require 'minitest/autorun'
require_relative 'polimorfismo'

class VeganCakeTest < Minitest::Test
  def test_1
    obj = BankFileImporter.new
    assert_equal 3, obj.import('itau')
  end

  def test_2
    obj = BankFileImporter.new
    assert_equal 206, obj.import('bradesco')
  end

  def test_3
    obj = BankFileImporter.new
    assert_equal -34.333333333333336, obj.import('bamerindos')
  end
end
