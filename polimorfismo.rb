class BankFileImporter
  def import(banco)
    @file = File.open("#{banco}.txt")

    result = 0

    @file.each_line do |line|
      if banco == 'itau'
        result += ItauParser.new.parse(line) + 1
      elsif banco == 'bradesco'
        result += BradescoParser.new.parse(line) * 2
      elsif 'bamerindos'
        result += BamerindosParser.new.parse(line).to_f / 3
      end
    end

    result
  end
end

class BankParser
  def parse(content)
    raise NotImplementedError
  end
end

class ItauParser < BankParser
  def parse(content)
    content.chars.map(&:to_i).reduce(&:*)
  end
end

class BradescoParser < BankParser
  def parse(content)
    content.chars.map(&:to_i).reduce(&:+)
  end
end

class BamerindosParser < BankParser
  def parse(content)
    content.chars.map(&:to_i).reduce(&:-)
  end
end
